import csv
import sys
import json
import codecs
import timeit
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

# Sentiment

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import tweepy
import preprocessor as pr
import nltk
from nltk.corpus import stopwords
import re
from collections import Counter


stop = set(stopwords.words('english'))

# Twitter thing 

import sys,getopt,datetime,codecs
import urllib,urllib2,json,re,datetime,sys,cookielib
from pyquery import PyQuery

class Tweet:
	
	def __init__(self):
		pass

class TweetCriteria:
	
	def __init__(self):
		self.maxTweets = 0
		
	def setUsername(self, username):
		self.username = username
		return self
		
	def setSince(self, since):
		self.since = since
		return self
	
	def setUntil(self, until):
		self.until = until
		return self
		
	def setQuerySearch(self, querySearch):
		self.querySearch = querySearch
		return self
		
	def setMaxTweets(self, maxTweets):
		self.maxTweets = maxTweets
		return self

	def setTopTweets(self, topTweets):
		self.topTweets = topTweets
		return self

class TweetManager:
	
	def __init__(self):
		pass
		
	@staticmethod
	def getTweets(tweetCriteria, receiveBuffer = None, bufferLength = 100):
		refreshCursor = ''
	
		results = []
		resultsAux = []
		cookieJar = cookielib.CookieJar()
		
		if hasattr(tweetCriteria, 'username') and (tweetCriteria.username.startswith("\'") or tweetCriteria.username.startswith("\"")) and (tweetCriteria.username.endswith("\'") or tweetCriteria.username.endswith("\"")):
			tweetCriteria.username = tweetCriteria.username[1:-1]

		active = True

		while active:
			json = TweetManager.getJsonReponse(tweetCriteria, refreshCursor, cookieJar)
			if len(json['items_html'].strip()) == 0:
				break

			refreshCursor = json['min_position']			
			tweets = PyQuery(json['items_html'])('div.js-stream-tweet')
			
			if len(tweets) == 0:
				break
			
			for tweetHTML in tweets:
				tweetPQ = PyQuery(tweetHTML)
				tweet = Tweet()
				
				usernameTweet = tweetPQ("span.username.js-action-profile-name b").text();
				txt = re.sub(r"\s+", " ", tweetPQ("p.js-tweet-text").text().replace('# ', '#').replace('@ ', '@'));
				retweets = int(tweetPQ("span.ProfileTweet-action--retweet span.ProfileTweet-actionCount").attr("data-tweet-stat-count").replace(",", ""));
				favorites = int(tweetPQ("span.ProfileTweet-action--favorite span.ProfileTweet-actionCount").attr("data-tweet-stat-count").replace(",", ""));
				dateSec = int(tweetPQ("small.time span.js-short-timestamp").attr("data-time"));
				id = tweetPQ.attr("data-tweet-id");
				permalink = tweetPQ.attr("data-permalink-path");
				
				geo = ''
				geoSpan = tweetPQ('span.Tweet-geo')
				if len(geoSpan) > 0:
					geo = geoSpan.attr('title')
				
				tweet.id = id
				tweet.permalink = 'https://twitter.com' + permalink
				tweet.username = usernameTweet
				tweet.text = txt
				tweet.date = datetime.datetime.fromtimestamp(dateSec)
				tweet.retweets = retweets
				tweet.favorites = favorites
				tweet.mentions = " ".join(re.compile('(@\\w*)').findall(tweet.text))
				tweet.hashtags = " ".join(re.compile('(#\\w*)').findall(tweet.text))
				tweet.geo = geo
				
				results.append(tweet)
				resultsAux.append(tweet)
				
				if receiveBuffer and len(resultsAux) >= bufferLength:
					receiveBuffer(resultsAux)
					resultsAux = []
				
				if tweetCriteria.maxTweets > 0 and len(results) >= tweetCriteria.maxTweets:
					active = False
					break
					
		
		if receiveBuffer and len(resultsAux) > 0:
			receiveBuffer(resultsAux)
		
		return results
	
	@staticmethod
	def getJsonReponse(tweetCriteria, refreshCursor, cookieJar):
		url = "https://twitter.com/i/search/timeline?f=tweets&q=%s&src=typd&max_position=%s"
		
		urlGetData = ''
		if hasattr(tweetCriteria, 'username'):
			urlGetData += ' from:' + tweetCriteria.username
			
		if hasattr(tweetCriteria, 'since'):
			urlGetData += ' since:' + tweetCriteria.since
			
		if hasattr(tweetCriteria, 'until'):
			urlGetData += ' until:' + tweetCriteria.until
			
		if hasattr(tweetCriteria, 'querySearch'):
			urlGetData += ' ' + tweetCriteria.querySearch

		if hasattr(tweetCriteria, 'topTweets'):
			if tweetCriteria.topTweets:
				url = "https://twitter.com/i/search/timeline?q=%s&src=typd&max_position=%s"

		url = url % (urllib.quote(urlGetData), refreshCursor)

		headers = [
			('Host', "twitter.com"),
			('User-Agent', "Mozilla/5.0 (Windows NT 6.1; Win64; x64)"),
			('Accept', "application/json, text/javascript, */*; q=0.01"),
			('Accept-Language', "de,en-US;q=0.7,en;q=0.3"),
			('X-Requested-With', "XMLHttpRequest"),
			('Referer', url),
			('Connection', "keep-alive")
		]

		opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookieJar))
		opener.addheaders = headers

		try:
			response = opener.open(url)
			jsonResponse = response.read()
		except:
			print "Twitter weird response. Try to see on browser: https://twitter.com/search?q=%s&src=typd" % urllib.quote(urlGetData)
			sys.exit()
			return
		
		dataJson = json.loads(jsonResponse)
		
		return dataJson		



# Create your views here.

def homepage(request):
	return render(request, 'twitter_scrapper/index.html')

# @csrf_exempt
def scrape_tweets(request):
	
	# Get tweets by query search
	query = str(request.GET.get('input'))
	# start_date = str(request.GET.get('startDate'))
	end_date = str(request.GET.get('endDate'))
	

	# query = 'Python Machine Learning'
	start_date = "2017-01-01"
	# end_date = "2017-04-18"
	
	print '\n\n',query, start_date, end_date,'\n\n'
	tweetCriteria = TweetCriteria().setQuerySearch(query).setSince(start_date).setUntil(end_date).setMaxTweets(500)
	start_time = timeit.default_timer()
	tweets = TweetManager.getTweets(tweetCriteria)
	print timeit.default_timer() - start_time
	
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="tweets.csv"'

	writer = csv.writer(response)
	writer.writerow(['Id', 'Tweet', 'Favorites', 'Retweets', 'Date & Time'])

	

	for i in tweets:
		writer.writerow([unicode(i.id).encode("utf-8"), unicode(i.text).encode("utf-8"), unicode(i.favorites).encode("utf-8"), unicode(i.retweets).encode("utf-8"), unicode(i.date.strftime("%Y-%m-%d %H:%M")).encode("utf-8")])
		print i.date.strftime("%Y-%m-%d %H:%M"), i.text, i.retweets, i.favorites

	return response


def fetch_tweets(query):

	consumer_key="36UBmR5MKNhfdOYlYHYdA"
	consumer_secret="bzvGib4mEpemMLHjgNUNwxFYikK6IECr7oufScNZ7g"
	access_token="127869815-d6EWusWl2rTwZtb78CEEk8xvOiyn2sLhRh0p8YfS"
	access_token_secret="ebJaRWFWILwTtSfB34BH9P2dNXAAIUkXKngvnwPRxE"

	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_token_secret)
	api = tweepy.API(auth)

	cleaned_tweets = []
	tweets = [unicode(pr.clean(unicode(t.text).encode("utf-8"))).encode("utf-8") for t in api.search(q=query,count="100")]
	
	for t in tweets:
		t = re.sub(r'[^\x00-\x7f]',r'', t) 
		st = ' '.join([i for i in t.lower().split() if i not in stop])
		sp_t = ' '.join([i for i in st.split() if i not in [":",";","-","(",")","="]])
		words = nltk.word_tokenize(sp_t)
		twt = ' '.join([word.lower() for word in words if word.isalpha()])
		cleaned_tweets.append(twt)

	return cleaned_tweets

def get_sentiment_scores(tweets):

	analyzer = SentimentIntensityAnalyzer()
	ex_pos, pos, neu, neg, ex_neg = 0, 0 , 0, 0, 0

	for t in tweets:
		vs = analyzer.polarity_scores(t)
		
		if vs['compound'] >= 0.8:
			ex_pos = ex_pos + 1

		elif 0.8 > vs['compound'] >= 0.4:
			pos = pos + 1

		elif 0.4 > vs['compound'] >= -0.2:
			neu = neu + 1

		elif -0.2 > vs['compound'] >= -0.7:
			neg = neg + 1

		elif -0.7 > vs['compound'] >= -1.0:
			ex_neg = ex_neg + 1

	d = [{"label":"Neutral", "value":neu}, {"label":"Negetive","value":neg}, {"label":"Positive","value":pos}, {"label":"Extremly Negetive","value":ex_neg}, {"label":"Extremly Positive","value":ex_pos} ]
	return d

def get_sentiment_data(request):
	q = str(request.GET.get('tweet_key'))
	data = get_sentiment_scores(fetch_tweets(q))
	# [{"label":"Neutral", "value":19}, {"label":"Negetive","value":5}, {"label":"Positive","value":113} ]

	return HttpResponse(json.dumps(data), content_type="application/json")

def getwordbins(words):
    cnt = Counter()
    for word in words:
        cnt[word] += 1
    return cnt

def get_word_cloud_data(request):

	q = str(request.GET.get('tweet_key'))
	tweets = filter(None, fetch_tweets(q))
	word_list = " ".join(tweets).split()
	words_list = [i for i in word_list if len(i)>2]
	data = []
	bins = getwordbins(words_list)
	for key, value in bins.most_common(50):
	    data.append({"key": str(key), "value": value})

	print data
	# data = [{"key": "Cat", "value": 216}, {"key": "fish", "value": 19}, {"key": "things", "value": 18}, {"key": "look", "value": 16}, {"key": "two", "value": 15}, {"key": "like", "value": 14}, {"key": "hat", "value": 14}, {"key": "Oh", "value": 13}, {"key": "mother", "value": 12}, {"key": "One", "value": 12}, {"key": "Now", "value": 12}, {"key": "Thing", "value": 12}, {"key": "house", "value": 10}, {"key": "fun", "value": 9}, {"key": "know", "value": 9}, {"key": "good", "value": 9}, {"key": "saw", "value": 9}, {"key": "bump", "value": 8}, {"key": "hold", "value": 7},{"key": "tail", "value": 1}, {"key": "dots", "value": 1}, {"key": "pink", "value": 1}, {"key": "white", "value": 1}, {"key": "kite", "value": 1}, {"key": "bed", "value": 1}, {"key": "bumps", "value": 1}, {"key": "jumps", "value": 1}, {"key": "kicks", "value": 1}, {"key": "hops", "value": 1}, {"key": "thumps", "value": 1}, {"key": "kinds", "value": 1}, {"key": "book", "value": 1}, {"key": "home", "value": 1}, {"key": "wood", "value": 1}, {"key": "hand", "value": 1}, {"key": "near", "value": 1}, {"key": "Think", "value": 1}, {"key": "rid", "value": 1}]

	return HttpResponse(json.dumps(data), content_type="application/json")

