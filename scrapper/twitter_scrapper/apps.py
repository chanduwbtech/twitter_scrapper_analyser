from __future__ import unicode_literals

from django.apps import AppConfig


class TwitterScrapperConfig(AppConfig):
    name = 'twitter_scrapper'
