from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

from twitter_scrapper import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.homepage),
    url(r'^tweets/', views.scrape_tweets),
    url(r'^getsenti/', views.get_sentiment_data),
    url(r'^getwords/', views.get_word_cloud_data),    
]

